export class Waste {
  /**
   * @param {number} weight le poid du déchet
   * @param {number} volume le volume que prend le déchet
   * @param {string} type la matière du déchet (plastic, glass, organic, other, paper)
   * @param {boolean} recyclable si le déchet est recyclable ou pas
   */
  constructor(weight, volume, type, recyclable) {
    this.weight = weight;
    this.type = type;
    this.volume = volume;
    this.recyclable = recyclable;
  }
}
