/// <reference types="Cypress" />


import { Smartbin } from "../src/SmartBin";
import { Waste } from "../src/Waste";

describe("SmartBin class test", () => {
  /**
   * @type SmartBin
   */
  let smartbin;
  let waste;
  beforeEach(() => {
    smartbin = new Smartbin(10, "blue");
    smartbin.content = [
      new Waste(100, 1, "plastic", false),
      new Waste(50, 1, "paper", true),
      new Waste(200, 1, "organic", false),
      new Waste(100, 1, "plastic", true),
      new Waste(50, 1, "paper", true),
      new Waste(500, 1, "glass", true)
    ];
    waste = new Waste(300, 1, "paper", true);
  });

  it("should empty the content of the bin", () => {
    smartbin.empty();
    expect(smartbin.content).to.be.empty;
  });

  it("should add trash in bin", () => {
    smartbin.add(waste);
    expect(smartbin.content).to.include(waste);
    expect(smartbin.content).to.have.length(7);
  });

  it("should not add trash above bin max volume", () => {
    waste.volume = 11;
    smartbin.add(waste);
    expect(smartbin.content).to.not.include(waste);
    expect(smartbin.content).to.have.length(6);
  });

  it("should returns an object with total weight of wastes", () => {
    let expectedResult = {
      grey: 100,
      yellow: 200,
      green: 200,
      glass: 500
    };
    let actualResult = smartbin.total();
    expect(expectedResult.grey).to.equal(actualResult.grey);
    expect(expectedResult.yellow).to.equal(actualResult.yellow);
    expect(expectedResult.green).to.equal(actualResult.green);
    expect(expectedResult.glass).to.equal(actualResult.glass);
  });

  it("should return total volume of waste", () => {
    expect(smartbin.totalVolume()).to.equal(6);
  });
});
