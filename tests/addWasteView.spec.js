/// <reference types="Cypress" />

describe("Waste from Page", () => {
  it("Sould visit page and add Waste to Bin", () => {
    cy.visit("http://localhost/js-test-smartbin/");
    cy.get("select").select("organic");
    cy.get('[type="checkbox"]').check();
    cy.get("#waste_volume").type("1");
    cy.get("#waste_weight").type("15");
    cy.get("#submit_waste").click();
    cy.get("#easyTitleFinder").contains("Actual content: 1");
  });

  //it("Should not add more waste that the waste weight max have", () => {

  //});
});
