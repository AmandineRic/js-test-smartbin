/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/SmartBin.js":
/*!*************************!*\
  !*** ./src/SmartBin.js ***!
  \*************************/
/*! exports provided: SmartBin */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SmartBin", function() { return SmartBin; });
/* harmony import */ var _Waste__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Waste */ "./src/Waste.js");


class SmartBin {
  /**
   * @param {number} volume le volume max de la poubelle
   * @param {string} color la couleur esthétique de la poubelle
   * @param {Waste[]} content le contenu de la poubelle
   */
  constructor(volume, color, content = []) {
    this.volume = volume;
    this.color = color;
    this.content = content;
  }
  /**
   * Méthode pour vider la poubelle
   */
  empty() {
    this.content = [];
  }
  /**
   * Méthode pour mettre un déchet à la poubelle
   * @param {Waste} waste le déchet à mettre à la poubelle
   */
  add(waste) {
    if (
      parseInt(waste.volume) + parseInt(this.totalVolume()) <=
      parseInt(this.volume)
    ) {
      this.content.push(waste);
    }
  }

  /**
   * Méthode qui calcul le volume actuel de tous les déchets contenus
   * dans la poubelle
   * @returns {number} le volume total
   */
  totalVolume() {
    let actualVolume = 0;
    for (const iterator of this.content) {
      actualVolume += iterator.volume;
    }
    return actualVolume;
    // return this.content.reduce((acc, cur) => acc+= cur.volume, 0);
  }

  /**
   * Méthode qui nous indique le poid des différents types de déchets
   * @returns {object} le bilan de ce qu'on a dans la poubelle
   */
  total() {
    let grey = 0;
    let yellow = 0;
    let green = 0;
    let glass = 0;
    for (const iterator of this.content) {
      if (!iterator.recyclable && iterator.type !== "organic") {
        grey += iterator.weight;
      }
      if (
        iterator.recyclable &&
        (iterator.type === "paper" || iterator.type === "plastic")
      ) {
        yellow += iterator.weight;
      }
      if (iterator.type === "organic") {
        green += iterator.weight;
      }
      if (iterator.type === "glass" && iterator.recyclable) {
        glass += iterator.weight;
      }
    }
    return { grey, yellow, green, glass };
    
  }
}


/***/ }),

/***/ "./src/ViewBin.js":
/*!************************!*\
  !*** ./src/ViewBin.js ***!
  \************************/
/*! exports provided: ViewBin */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewBin", function() { return ViewBin; });
/* harmony import */ var _SmartBin__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SmartBin */ "./src/SmartBin.js");


class ViewBin {
         /**
          *
          * @param {string} selector Sélécteur css de l'élément où mettre l'affichage
          * @param {number} volume le volume de la poubelle
          * @param {string} color  la couleur de la poubelle
          */
         constructor(selector, volume, color) {
           this.selector = selector;
           this.smartBin = new _SmartBin__WEBPACK_IMPORTED_MODULE_0__["SmartBin"](volume, color);
         }

         /**
          * Méthode qui génère le html de la poubelle en propriété
          * @returns {Element} L'élément html généré
          */
         draw() {
           let target = document.querySelector(this.selector);
           target.innerHTML = "";

           let element = document.createElement("div");
           element.classList.add("smart-bin");
           element.style.backgroundColor = this.color;

           let div = document.createElement("div");
           div.textContent = "Actual Volume : " + this.smartBin.totalVolume();
           div.id = "easyTitleFinder";

           element.appendChild(div);
           this.drawWeight(element);

           target.appendChild(element);

           return element;
         }

         /**
          * Méthode qui génère le html représentant le poid des déchets
          * @param {Element} element L'élément html où append le poid des déchets
          */
         drawWeight(element) {
           let total = this.smartBin.total();
           let pGrey = document.createElement("p");
           pGrey.textContent = "Grey : " + total.grey + " gr";
           pGrey.setAttribute("id", "grey_weight");
           element.appendChild(pGrey);

           let pGlass = document.createElement("p");
           pGlass.textContent = "Glass : " + total.glass + " gr";
           pGlass.setAttribute("id", "glass_weight");
           element.appendChild(pGlass);

           let pGreen = document.createElement("p");
           pGreen.textContent = "Green : " + total.green + " gr";
           pGreen.setAttribute("id", "green_weight");
           element.appendChild(pGreen);

           let pYellow = document.createElement("p");
           pYellow.textContent = "Yellow : " + total.yellow + " gr";
           pYellow.setAttribute("id", "yellow_weight");
           element.appendChild(pYellow);
         }
       }


/***/ }),

/***/ "./src/Waste.js":
/*!**********************!*\
  !*** ./src/Waste.js ***!
  \**********************/
/*! exports provided: Waste */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Waste", function() { return Waste; });
class Waste {
  /**
   * @param {number} weight le poid du déchet
   * @param {number} volume le volume que prend le déchet
   * @param {string} type la matière du déchet (plastic, glass, organic, other, paper)
   * @param {boolean} recyclable si le déchet est recyclable ou pas
   */
  constructor(weight, volume, type, recyclable) {
    this.weight = weight;
    this.type = type;
    this.volume = volume;
    this.recyclable = recyclable;
  }
}


/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ViewBin__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ViewBin */ "./src/ViewBin.js");
/* harmony import */ var _Waste__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Waste */ "./src/Waste.js");




let view = new _ViewBin__WEBPACK_IMPORTED_MODULE_0__["ViewBin"]("#target", 10, "blue");

view.draw();

console.log("coucou1");

// Récupérer les valeurs des inputs de notre form
let inputType = document.querySelector("#waste_type");
let inputVolume = document.querySelector("#waste_volume");
let inputRecyclable = document.querySelector("#waste_recyclable");
let inputWeight = document.querySelector("#waste_weight");
let buttonSubmit = document.querySelector("#submit_waste");

console.log("coucou2");

buttonSubmit.addEventListener("click", function() {
  //créer un new waste, avec les valeurs qu'on récupère au click
  let newWaste = new _Waste__WEBPACK_IMPORTED_MODULE_1__["Waste"](
    parseInt(inputWeight.value),
    parseInt(inputVolume.value),
    inputType.value,
    inputRecyclable.value
  );
  console.log(newWaste);

  view.smartBin.add(newWaste);

  console.log(view.smartBin.content);

  view.draw();
});


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL1NtYXJ0QmluLmpzIiwid2VicGFjazovLy8uL3NyYy9WaWV3QmluLmpzIiwid2VicGFjazovLy8uL3NyYy9XYXN0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUFBO0FBQUE7QUFBZ0M7O0FBRXpCO0FBQ1A7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxNQUFNO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE9BQU87QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxlQUFlLE9BQU87QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVk7O0FBRVo7QUFDQTs7Ozs7Ozs7Ozs7OztBQzNFQTtBQUFBO0FBQUE7QUFBc0M7O0FBRS9CO0FBQ1A7QUFDQTtBQUNBLG9CQUFvQixPQUFPO0FBQzNCLG9CQUFvQixPQUFPO0FBQzNCLG9CQUFvQixPQUFPO0FBQzNCO0FBQ0E7QUFDQTtBQUNBLCtCQUErQixrREFBUTtBQUN2Qzs7QUFFQTtBQUNBO0FBQ0Esc0JBQXNCLFFBQVE7QUFDOUI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG9CQUFvQixRQUFRO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ2hFQTtBQUFBO0FBQU87QUFDUDtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLE9BQU87QUFDcEIsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDYkE7QUFBQTtBQUFBO0FBQW9DO0FBQ0o7OztBQUdoQyxlQUFlLGdEQUFPOztBQUV0Qjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLHFCQUFxQiw0Q0FBSztBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQSxDQUFDIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC5qc1wiKTtcbiIsImltcG9ydCB7IFdhc3RlIH0gZnJvbSBcIi4vV2FzdGVcIjtcblxuZXhwb3J0IGNsYXNzIFNtYXJ0QmluIHtcbiAgLyoqXG4gICAqIEBwYXJhbSB7bnVtYmVyfSB2b2x1bWUgbGUgdm9sdW1lIG1heCBkZSBsYSBwb3ViZWxsZVxuICAgKiBAcGFyYW0ge3N0cmluZ30gY29sb3IgbGEgY291bGV1ciBlc3Row6l0aXF1ZSBkZSBsYSBwb3ViZWxsZVxuICAgKiBAcGFyYW0ge1dhc3RlW119IGNvbnRlbnQgbGUgY29udGVudSBkZSBsYSBwb3ViZWxsZVxuICAgKi9cbiAgY29uc3RydWN0b3Iodm9sdW1lLCBjb2xvciwgY29udGVudCA9IFtdKSB7XG4gICAgdGhpcy52b2x1bWUgPSB2b2x1bWU7XG4gICAgdGhpcy5jb2xvciA9IGNvbG9yO1xuICAgIHRoaXMuY29udGVudCA9IGNvbnRlbnQ7XG4gIH1cbiAgLyoqXG4gICAqIE3DqXRob2RlIHBvdXIgdmlkZXIgbGEgcG91YmVsbGVcbiAgICovXG4gIGVtcHR5KCkge1xuICAgIHRoaXMuY29udGVudCA9IFtdO1xuICB9XG4gIC8qKlxuICAgKiBNw6l0aG9kZSBwb3VyIG1ldHRyZSB1biBkw6ljaGV0IMOgIGxhIHBvdWJlbGxlXG4gICAqIEBwYXJhbSB7V2FzdGV9IHdhc3RlIGxlIGTDqWNoZXQgw6AgbWV0dHJlIMOgIGxhIHBvdWJlbGxlXG4gICAqL1xuICBhZGQod2FzdGUpIHtcbiAgICBpZiAoXG4gICAgICBwYXJzZUludCh3YXN0ZS52b2x1bWUpICsgcGFyc2VJbnQodGhpcy50b3RhbFZvbHVtZSgpKSA8PVxuICAgICAgcGFyc2VJbnQodGhpcy52b2x1bWUpXG4gICAgKSB7XG4gICAgICB0aGlzLmNvbnRlbnQucHVzaCh3YXN0ZSk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIE3DqXRob2RlIHF1aSBjYWxjdWwgbGUgdm9sdW1lIGFjdHVlbCBkZSB0b3VzIGxlcyBkw6ljaGV0cyBjb250ZW51c1xuICAgKiBkYW5zIGxhIHBvdWJlbGxlXG4gICAqIEByZXR1cm5zIHtudW1iZXJ9IGxlIHZvbHVtZSB0b3RhbFxuICAgKi9cbiAgdG90YWxWb2x1bWUoKSB7XG4gICAgbGV0IGFjdHVhbFZvbHVtZSA9IDA7XG4gICAgZm9yIChjb25zdCBpdGVyYXRvciBvZiB0aGlzLmNvbnRlbnQpIHtcbiAgICAgIGFjdHVhbFZvbHVtZSArPSBpdGVyYXRvci52b2x1bWU7XG4gICAgfVxuICAgIHJldHVybiBhY3R1YWxWb2x1bWU7XG4gICAgLy8gcmV0dXJuIHRoaXMuY29udGVudC5yZWR1Y2UoKGFjYywgY3VyKSA9PiBhY2MrPSBjdXIudm9sdW1lLCAwKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBNw6l0aG9kZSBxdWkgbm91cyBpbmRpcXVlIGxlIHBvaWQgZGVzIGRpZmbDqXJlbnRzIHR5cGVzIGRlIGTDqWNoZXRzXG4gICAqIEByZXR1cm5zIHtvYmplY3R9IGxlIGJpbGFuIGRlIGNlIHF1J29uIGEgZGFucyBsYSBwb3ViZWxsZVxuICAgKi9cbiAgdG90YWwoKSB7XG4gICAgbGV0IGdyZXkgPSAwO1xuICAgIGxldCB5ZWxsb3cgPSAwO1xuICAgIGxldCBncmVlbiA9IDA7XG4gICAgbGV0IGdsYXNzID0gMDtcbiAgICBmb3IgKGNvbnN0IGl0ZXJhdG9yIG9mIHRoaXMuY29udGVudCkge1xuICAgICAgaWYgKCFpdGVyYXRvci5yZWN5Y2xhYmxlICYmIGl0ZXJhdG9yLnR5cGUgIT09IFwib3JnYW5pY1wiKSB7XG4gICAgICAgIGdyZXkgKz0gaXRlcmF0b3Iud2VpZ2h0O1xuICAgICAgfVxuICAgICAgaWYgKFxuICAgICAgICBpdGVyYXRvci5yZWN5Y2xhYmxlICYmXG4gICAgICAgIChpdGVyYXRvci50eXBlID09PSBcInBhcGVyXCIgfHwgaXRlcmF0b3IudHlwZSA9PT0gXCJwbGFzdGljXCIpXG4gICAgICApIHtcbiAgICAgICAgeWVsbG93ICs9IGl0ZXJhdG9yLndlaWdodDtcbiAgICAgIH1cbiAgICAgIGlmIChpdGVyYXRvci50eXBlID09PSBcIm9yZ2FuaWNcIikge1xuICAgICAgICBncmVlbiArPSBpdGVyYXRvci53ZWlnaHQ7XG4gICAgICB9XG4gICAgICBpZiAoaXRlcmF0b3IudHlwZSA9PT0gXCJnbGFzc1wiICYmIGl0ZXJhdG9yLnJlY3ljbGFibGUpIHtcbiAgICAgICAgZ2xhc3MgKz0gaXRlcmF0b3Iud2VpZ2h0O1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4geyBncmV5LCB5ZWxsb3csIGdyZWVuLCBnbGFzcyB9O1xuICAgIFxuICB9XG59XG4iLCJpbXBvcnQgeyBTbWFydEJpbiB9IGZyb20gXCIuL1NtYXJ0QmluXCI7XG5cbmV4cG9ydCBjbGFzcyBWaWV3QmluIHtcbiAgICAgICAgIC8qKlxuICAgICAgICAgICpcbiAgICAgICAgICAqIEBwYXJhbSB7c3RyaW5nfSBzZWxlY3RvciBTw6lsw6ljdGV1ciBjc3MgZGUgbCfDqWzDqW1lbnQgb8O5IG1ldHRyZSBsJ2FmZmljaGFnZVxuICAgICAgICAgICogQHBhcmFtIHtudW1iZXJ9IHZvbHVtZSBsZSB2b2x1bWUgZGUgbGEgcG91YmVsbGVcbiAgICAgICAgICAqIEBwYXJhbSB7c3RyaW5nfSBjb2xvciAgbGEgY291bGV1ciBkZSBsYSBwb3ViZWxsZVxuICAgICAgICAgICovXG4gICAgICAgICBjb25zdHJ1Y3RvcihzZWxlY3Rvciwgdm9sdW1lLCBjb2xvcikge1xuICAgICAgICAgICB0aGlzLnNlbGVjdG9yID0gc2VsZWN0b3I7XG4gICAgICAgICAgIHRoaXMuc21hcnRCaW4gPSBuZXcgU21hcnRCaW4odm9sdW1lLCBjb2xvcik7XG4gICAgICAgICB9XG5cbiAgICAgICAgIC8qKlxuICAgICAgICAgICogTcOpdGhvZGUgcXVpIGfDqW7DqHJlIGxlIGh0bWwgZGUgbGEgcG91YmVsbGUgZW4gcHJvcHJpw6l0w6lcbiAgICAgICAgICAqIEByZXR1cm5zIHtFbGVtZW50fSBMJ8OpbMOpbWVudCBodG1sIGfDqW7DqXLDqVxuICAgICAgICAgICovXG4gICAgICAgICBkcmF3KCkge1xuICAgICAgICAgICBsZXQgdGFyZ2V0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0aGlzLnNlbGVjdG9yKTtcbiAgICAgICAgICAgdGFyZ2V0LmlubmVySFRNTCA9IFwiXCI7XG5cbiAgICAgICAgICAgbGV0IGVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xuICAgICAgICAgICBlbGVtZW50LmNsYXNzTGlzdC5hZGQoXCJzbWFydC1iaW5cIik7XG4gICAgICAgICAgIGVsZW1lbnQuc3R5bGUuYmFja2dyb3VuZENvbG9yID0gdGhpcy5jb2xvcjtcblxuICAgICAgICAgICBsZXQgZGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKTtcbiAgICAgICAgICAgZGl2LnRleHRDb250ZW50ID0gXCJBY3R1YWwgVm9sdW1lIDogXCIgKyB0aGlzLnNtYXJ0QmluLnRvdGFsVm9sdW1lKCk7XG4gICAgICAgICAgIGRpdi5pZCA9IFwiZWFzeVRpdGxlRmluZGVyXCI7XG5cbiAgICAgICAgICAgZWxlbWVudC5hcHBlbmRDaGlsZChkaXYpO1xuICAgICAgICAgICB0aGlzLmRyYXdXZWlnaHQoZWxlbWVudCk7XG5cbiAgICAgICAgICAgdGFyZ2V0LmFwcGVuZENoaWxkKGVsZW1lbnQpO1xuXG4gICAgICAgICAgIHJldHVybiBlbGVtZW50O1xuICAgICAgICAgfVxuXG4gICAgICAgICAvKipcbiAgICAgICAgICAqIE3DqXRob2RlIHF1aSBnw6luw6hyZSBsZSBodG1sIHJlcHLDqXNlbnRhbnQgbGUgcG9pZCBkZXMgZMOpY2hldHNcbiAgICAgICAgICAqIEBwYXJhbSB7RWxlbWVudH0gZWxlbWVudCBMJ8OpbMOpbWVudCBodG1sIG/DuSBhcHBlbmQgbGUgcG9pZCBkZXMgZMOpY2hldHNcbiAgICAgICAgICAqL1xuICAgICAgICAgZHJhd1dlaWdodChlbGVtZW50KSB7XG4gICAgICAgICAgIGxldCB0b3RhbCA9IHRoaXMuc21hcnRCaW4udG90YWwoKTtcbiAgICAgICAgICAgbGV0IHBHcmV5ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInBcIik7XG4gICAgICAgICAgIHBHcmV5LnRleHRDb250ZW50ID0gXCJHcmV5IDogXCIgKyB0b3RhbC5ncmV5ICsgXCIgZ3JcIjtcbiAgICAgICAgICAgcEdyZXkuc2V0QXR0cmlidXRlKFwiaWRcIiwgXCJncmV5X3dlaWdodFwiKTtcbiAgICAgICAgICAgZWxlbWVudC5hcHBlbmRDaGlsZChwR3JleSk7XG5cbiAgICAgICAgICAgbGV0IHBHbGFzcyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJwXCIpO1xuICAgICAgICAgICBwR2xhc3MudGV4dENvbnRlbnQgPSBcIkdsYXNzIDogXCIgKyB0b3RhbC5nbGFzcyArIFwiIGdyXCI7XG4gICAgICAgICAgIHBHbGFzcy5zZXRBdHRyaWJ1dGUoXCJpZFwiLCBcImdsYXNzX3dlaWdodFwiKTtcbiAgICAgICAgICAgZWxlbWVudC5hcHBlbmRDaGlsZChwR2xhc3MpO1xuXG4gICAgICAgICAgIGxldCBwR3JlZW4gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwicFwiKTtcbiAgICAgICAgICAgcEdyZWVuLnRleHRDb250ZW50ID0gXCJHcmVlbiA6IFwiICsgdG90YWwuZ3JlZW4gKyBcIiBnclwiO1xuICAgICAgICAgICBwR3JlZW4uc2V0QXR0cmlidXRlKFwiaWRcIiwgXCJncmVlbl93ZWlnaHRcIik7XG4gICAgICAgICAgIGVsZW1lbnQuYXBwZW5kQ2hpbGQocEdyZWVuKTtcblxuICAgICAgICAgICBsZXQgcFllbGxvdyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJwXCIpO1xuICAgICAgICAgICBwWWVsbG93LnRleHRDb250ZW50ID0gXCJZZWxsb3cgOiBcIiArIHRvdGFsLnllbGxvdyArIFwiIGdyXCI7XG4gICAgICAgICAgIHBZZWxsb3cuc2V0QXR0cmlidXRlKFwiaWRcIiwgXCJ5ZWxsb3dfd2VpZ2h0XCIpO1xuICAgICAgICAgICBlbGVtZW50LmFwcGVuZENoaWxkKHBZZWxsb3cpO1xuICAgICAgICAgfVxuICAgICAgIH1cbiIsImV4cG9ydCBjbGFzcyBXYXN0ZSB7XG4gIC8qKlxuICAgKiBAcGFyYW0ge251bWJlcn0gd2VpZ2h0IGxlIHBvaWQgZHUgZMOpY2hldFxuICAgKiBAcGFyYW0ge251bWJlcn0gdm9sdW1lIGxlIHZvbHVtZSBxdWUgcHJlbmQgbGUgZMOpY2hldFxuICAgKiBAcGFyYW0ge3N0cmluZ30gdHlwZSBsYSBtYXRpw6hyZSBkdSBkw6ljaGV0IChwbGFzdGljLCBnbGFzcywgb3JnYW5pYywgb3RoZXIsIHBhcGVyKVxuICAgKiBAcGFyYW0ge2Jvb2xlYW59IHJlY3ljbGFibGUgc2kgbGUgZMOpY2hldCBlc3QgcmVjeWNsYWJsZSBvdSBwYXNcbiAgICovXG4gIGNvbnN0cnVjdG9yKHdlaWdodCwgdm9sdW1lLCB0eXBlLCByZWN5Y2xhYmxlKSB7XG4gICAgdGhpcy53ZWlnaHQgPSB3ZWlnaHQ7XG4gICAgdGhpcy50eXBlID0gdHlwZTtcbiAgICB0aGlzLnZvbHVtZSA9IHZvbHVtZTtcbiAgICB0aGlzLnJlY3ljbGFibGUgPSByZWN5Y2xhYmxlO1xuICB9XG59XG4iLCJpbXBvcnQgeyBWaWV3QmluIH0gZnJvbSBcIi4vVmlld0JpblwiO1xuaW1wb3J0IHsgV2FzdGUgfSBmcm9tIFwiLi9XYXN0ZVwiO1xuXG5cbmxldCB2aWV3ID0gbmV3IFZpZXdCaW4oXCIjdGFyZ2V0XCIsIDEwLCBcImJsdWVcIik7XG5cbnZpZXcuZHJhdygpO1xuXG5jb25zb2xlLmxvZyhcImNvdWNvdTFcIik7XG5cbi8vIFLDqWN1cMOpcmVyIGxlcyB2YWxldXJzIGRlcyBpbnB1dHMgZGUgbm90cmUgZm9ybVxubGV0IGlucHV0VHlwZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjd2FzdGVfdHlwZVwiKTtcbmxldCBpbnB1dFZvbHVtZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjd2FzdGVfdm9sdW1lXCIpO1xubGV0IGlucHV0UmVjeWNsYWJsZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjd2FzdGVfcmVjeWNsYWJsZVwiKTtcbmxldCBpbnB1dFdlaWdodCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjd2FzdGVfd2VpZ2h0XCIpO1xubGV0IGJ1dHRvblN1Ym1pdCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjc3VibWl0X3dhc3RlXCIpO1xuXG5jb25zb2xlLmxvZyhcImNvdWNvdTJcIik7XG5cbmJ1dHRvblN1Ym1pdC5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24oKSB7XG4gIC8vY3LDqWVyIHVuIG5ldyB3YXN0ZSwgYXZlYyBsZXMgdmFsZXVycyBxdSdvbiByw6ljdXDDqHJlIGF1IGNsaWNrXG4gIGxldCBuZXdXYXN0ZSA9IG5ldyBXYXN0ZShcbiAgICBwYXJzZUludChpbnB1dFdlaWdodC52YWx1ZSksXG4gICAgcGFyc2VJbnQoaW5wdXRWb2x1bWUudmFsdWUpLFxuICAgIGlucHV0VHlwZS52YWx1ZSxcbiAgICBpbnB1dFJlY3ljbGFibGUudmFsdWVcbiAgKTtcbiAgY29uc29sZS5sb2cobmV3V2FzdGUpO1xuXG4gIHZpZXcuc21hcnRCaW4uYWRkKG5ld1dhc3RlKTtcblxuICBjb25zb2xlLmxvZyh2aWV3LnNtYXJ0QmluLmNvbnRlbnQpO1xuXG4gIHZpZXcuZHJhdygpO1xufSk7XG4iXSwic291cmNlUm9vdCI6IiJ9